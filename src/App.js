import React from 'react';
import { gql } from 'apollo-boost';
import { Query, graphql } from 'react-apollo';
import { compose } from 'recompose';

// const GET_MOVIES = gql`
//   query {
//     allMovies {
//       id
//       title
//     }
//   }
// `

// const App = () => (
//   <Query query={GET_MOVIES}>
//     {({ loading, error, data }) => {
//       if (loading) return <div>Loading...</div>;
//       if (error) return <div>Error :(</div>;

//       return (
//         <div>
//           {data.allMovies.map(movie => (
//             <div key={movie.id}>{movie.title}</div>
//           ))}
//         </div>
//       )
//     }}
//   </Query>
// )

// export default App
















// ======================================================

const App = ({ data: { loading, error, allMovies } }) => {
  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error :(</div>;

  return (
    <div>
      {allMovies.map(movie => (
        <div key={movie.id}>{movie.title}</div>
      ))}
    </div>
  )
}

export default graphql(gql`
  query {
    allMovies {
      id
      title
    }
  }
`)(App)












