import React from 'react';
import { render } from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import App from './App'

// Pass your GraphQL endpoint to uri
const client = new ApolloClient({ uri: 'https://api.graph.cool/simple/v1/movies' });

const ApolloApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

render(ApolloApp(), document.getElementById('root'));